import React, {Component} from 'react';
var adminActions = require('actions/admin');
var {Link, IndexLink} = require('react-router');
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';

import dataProvider from 'dataProvider';
const uuidV1 = require('uuid/v1');

import TextField from 'material-ui/TextField';
import Filter from './../Filter';

const userInfoName = ['doctor', 'name'];
const userInfoLastname = ['doctor', 'lastname'];
const searchDateBetweenInvoices = {branch: ['data'], element:['month']};

const columnsStyle = { width : 'calc( 100% / 2 - 1px ) ', display: 'inline-block'};

import { DateRange } from 'react-date-range';
import moment from 'moment';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import FileFileDownload from 'material-ui/svg-icons/file/file-download';
export class Facture extends Component {
    constructor(props, context) {
        super(props, context);
        this.state={
            loaded: false,
            searchName: false,
            searchLastname: false,
            searchId: false,
            showCalendar: false,
            selectedRows: {},
            filter: new Filter(this), 
        };
        this.redirectToPaymentPage = this.redirectToPaymentPage.bind(this);
        this.handleSelectDate = this.handleSelectDate.bind(this);
        this.floatSelectedIds = this.floatSelectedIds.bind(this);
        this.showChoosedFactures = this.showChoosedFactures.bind(this);
    }

    handleSelectDate(date){
        this.setState({dateSearch: date});
        this.setState(this.state.filter.setValueByKey(searchDateBetweenInvoices, date));
        console.log(date); // Momentjs object
    }
    showChoosedFactures(){
        window.open(dataProvider.INVOICE_PDF_PAGE + dataProvider.USER_API_ADDITIONAL_PATH + '&id='+this.floatSelectedIds().join(',')+ '&type=doctor');
    }
    componentWillMount() {
        console.log('try to get doctor factures');
        this.props.dispatch(adminActions.getAllDoctorFactures())
            .then((resp)=>{
                this.setState({loaded: true});
                console.log('Finished get doctor factures', resp);
            });
    }
    redirectToPaymentPage(elem){
        window.open(dataProvider.INVOICE_PDF_PAGE + dataProvider.USER_API_ADDITIONAL_PATH + '&id='+elem.id + '&type=doctor');
    }
    getSelectedRows(){
        if(!this.state.selectedRows.length){
            return null;
        }else{
            var ids = [];
            this.state.selectedRows.forEach((data)=>{
                data.forEach(e=>ids.push(data));
            });
            return (
                <div>
                    {ids.join(',')}
                </div>
            );
        }
    }
    floatSelectedIds(){
        if(!Object.keys(this.state.selectedRows).length){
            return [];
        }else{
            var ids = [];
            Object.keys(this.state.selectedRows).forEach((key)=>{
                this.state.selectedRows[key].forEach(e=>ids.push(e));
            });
            return ids;
        }
    }
    render() {
        if (!this.state.loaded)
            return (
                <div  className="infoLineLoading">
                    Chargement...
                </div>
            );

        var factures = this
            .state  
            .filter
            .setFilteredItems(this.props.factures)
            .filterAttribute(userInfoName, false, true)
            .filterAttribute(userInfoLastname, false, true)
            .searchInBranch(searchDateBetweenInvoices, false, false, (valueObj, value)=>{
                var date = moment().year(parseInt(valueObj.split('-')[1])).month(parseInt(valueObj.split('-')[0])-1).date(1);
                // debugger;
                if(value.endDate.diff(value.startDate)<(60*60*24*1000))
                    return true;
                return (date>=value.startDate && date<value.endDate);
            })
            .getFilteredItems();

        if (typeof this.props.factures === 'undefined' || this.props.factures.length == 0)
            return (
                <div  className="infoLineLoading">
                    Non factures
                </div>
            );


        return (
            <div>
                <h2>Mes factures</h2>

                {
                    this.floatSelectedIds().length? 
                        <MuiThemeProvider>
                            <RaisedButton
                                    label={`Télécharger `}
                                    onClick={() =>{this.showChoosedFactures()}}
                                />
                        </MuiThemeProvider>
                    : null
                }
                <div style={{ width: '100%'}}>
                    <div style={columnsStyle}>
                         <MuiThemeProvider>
                            <TextField
                                style={{'vertical-align':'bottom'}}
                                hintText="Prénom du doctor"
                                onChange={(event, value) => this.setState(this.state.filter.setValueByKey(userInfoName, value))}
                            />
                        </MuiThemeProvider>
                    </div>
                    <div style={columnsStyle}>
                         <MuiThemeProvider>
                            <TextField
                                style={{'vertical-align':'bottom'}}
                                hintText="Nom du doctor"
                                onChange={(event, value) => this.setState(this.state.filter.setValueByKey(userInfoLastname, value))}
                            />
                        </MuiThemeProvider>
                    </div>
                    <div style={columnsStyle}>
                         <MuiThemeProvider>
                            <RaisedButton
                                label={(this.state.showCalendar) ? `Cacher calendrier` : `Calendrier`}
                                primary={true}
                                onClick={() =>{this.setState({showCalendar: !this.state.showCalendar})}}
                             />
                        </MuiThemeProvider>
                    </div>
                </div>

                {
                    this.state.showCalendar?
                    <div>
                        <DateRange
                            lang={`fr`}
                            onInit={(date)=>{
                                this.handleSelectDate(date);
                            }}
                            onChange={(date)=>{
                                this.handleSelectDate(date);
                            }}
                        />
                    </div>:null
                }
                    {factures.map((elem, index)=> {
                        return (
                            <div>
                                <div style={{'font-size': '17px', 'text-decoration': 'underline'}}>
                                    {elem.doctor.lastname} - {elem.doctor.name}</div>
                                <div>{elem.doctor.email}</div>
                                <MuiThemeProvider>
                                    <Table
                                        adjustForCheckbox={false}
                                        fixedHeader={true}
                                        selectable={true}
                                        multiSelectable={true}
                                        onRowSelection={(data)=>{
                                            console.log(data);
                                            var selectedRows = Object.assign({}, this.state.selectedRows);
                                            if(!data.length){
                                                delete selectedRows[elem.doctor.id];
                                            }else{
                                                var ids = data.map(function(data, index){
                                                    return elem.data[data].id;
                                                });
                                                selectedRows[elem.doctor.id] = ids;
                                            }
                                            this.setState({selectedRows});
                                            console.log(elem.data);
                                        }}
                                    >
                                        <TableHeader
                                            adjustForCheckbox={false}
                                            displaySelectAll={false}
                                        >
                                            <TableRow>
                                                <TableHeaderColumn>Montant TTC</TableHeaderColumn>
                                                <TableHeaderColumn>Nombre de téléconseils</TableHeaderColumn>
                                                <TableHeaderColumn>Mois</TableHeaderColumn>
                                                <TableHeaderColumn>&nbsp;</TableHeaderColumn>
                                            </TableRow>
                                        </TableHeader>
                                        <TableBody 
                                            deselectOnClickaway={false}
                                            displayRowCheckbox={true}
                                            selectedRows
                                            stripedRows={false} >
                                            {
                                                elem.data.map((facture, factureIndex)=>{
                                                    return (
                                                        <TableRow
                                                            key={uuidV1()}
                                                            selected={this.floatSelectedIds().some(x=>x == facture.id)}
                                                            // onMouseUp={()=> { this.setActiveRow(elem) } }
                                                        >
                                                            <TableRowColumn>{facture.costFormated}</TableRowColumn>
                                                            <TableRowColumn>{facture.count}</TableRowColumn>
                                                            <TableRowColumn>{facture.month}</TableRowColumn>
                                                            <TableRowColumn style={{'width':'20%'}}>
                                                                <RaisedButton
                                                                    // label={`Télécharger`}
                                                                    icon={<FileFileDownload />}
                                                                    //style={{'width':'100%'}}
                                                                    primary={true}
                                                                    onClick={() =>{this.redirectToPaymentPage(facture)}}
                                                                />
                                                            </TableRowColumn>
                                                        </TableRow>
                                                    )
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </MuiThemeProvider>
                            </div>
                        );
                    })}
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    console.log('factursDoctor', state.factursDoctor);
    return {
        factures: state.adminFactursDoctor,
        ...ownProps
    };
}
export default connect(mapStateToProps)(Facture);