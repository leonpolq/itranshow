<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 5/31/2017
 * Time: 8:48 AM
 */
class WageAgreementIDCC1747
	extends
		WageAgreementCommonAbstract
	implements
		DefaultConfigurationInterface,
		CalulationProviderInterface,
		HourRateCalculationInterface
{
	const BANK_HOLIDAYS = 'bankHolidays';
	const SUNDAY = 'sunday';
	const HS_125 = 'hs125';
	const HS_150 = 'hs150';
	const HS_MORE_10 = 'HS_more_10';
	const NIGHT_HOURS = 'nightHours';
	use DefaultValuesProviderTrait;

	/** @var  Restaurant $_restaurantModel */
	protected $_restaurantModel;
	protected $_wageModel;
	/** @var WageAgreementIDCC1747Calculations  */
	protected $_calculationProvider;
	/** @var array[] array  */
	private $_hourRateElements = [];
	protected $_hourRates = [];
//	private $_hourRates = [];
	/**
	 * Inject dataProvider = RestaurantFactures
	 * WageAgreement constructor.
	 * @param Restaurant $restaurant
	 */
	public function __construct(Restaurant $restaurant) {
		$this->_restaurantModel = $restaurant;
		$this->_wageModel =  $this->_restaurantModel->WageAgreement;
		$this->_calculationProvider = new WageAgreementIDCC1747Calculations();
		$this->initDefaultValuesStorage()
			->setDefaultValues();
		parent:: __construct($restaurant);
	}

	/**
	 * @return WageAgreementIDCC1747Calculations
	 */
	public function getCalculationProvider(){
		return $this->_calculationProvider;
	}




	public function getFullTitle() {
		return $this->_wageModel->name.' '.$this->_wageModel->description;
	}

	public function getTitle() {
		return $this->_wageModel->name;
	}

	/**
	 *
	 */
	private function setDefaultValues() {
		$c = new CDbCriteria();
		$c->with = ['wageAgreementConfigurationKeys'];
		$c->compare('t.wage_agreement_id', $this->_wageModel->id);
		$models = WageAgreementConfiguration::model()->findAll($c);
		$values = array_column(array_map(function(WageAgreementConfiguration $a){
			$this->setValueDefault($a->wageAgreementConfigurationKeys->name, $a->value);
			return [$a->value, $a->wageAgreementConfigurationKeys->name];
		}, $models), '1', '0');
		//$values = CHtml::listData($models, 'wageAgreementConfigurationKeys.name', 't.value');
		//Tools::dump($values);

		$this->setValueDefault('user.weekly_working_hour[full_time]', 35);
	}

	/**
	 * set a half empty array without some values
	 * @param $rate
	 * @param HourRateElementCalculationInterface $elementCalculation
	 */
	private function _addCaltulationHourRateElement($rate, HourRateElementCalculationInterface $elementCalculation, $publicAlias){
	    if(!isset($this->_hourRateElements[$elementCalculation->getDate()]))
            $this->_hourRateElements[$elementCalculation->getDate()]=[];
		if(!isset($this->_hourRateElements[$elementCalculation->getDate()][$publicAlias])){
			$this->_hourRateElements[$elementCalculation->getDate()]=array_replace($this->_hourRateElements[$elementCalculation->getDate()], [$publicAlias=>[]]);
			$this->_hourRateElements[$elementCalculation->getDate()] = array_replace($this->_hourRateElements[$elementCalculation->getDate()] ,
				[$publicAlias =>
					[
						'rate'=>$rate,
						'class'=>$elementCalculation,
						'publicAlias'=>$publicAlias,
						'calculated'=>false,
					]
				]
			);
		}
	}

	/**
	 * Initialize half empty array to next calculations
	 * @param User $user
	 * @param $date
	 */
	private function _registerCalculations(User $user, $date) {
		$this->_addCaltulationHourRateElement((float)$this->getValueDefault('wage.WageAgreementIDCC1747HourRateBankHolidays'), new WageAgreementIDCC1747HourRateBankHolidays($user, $date), self::BANK_HOLIDAYS);
		$this->_addCaltulationHourRateElement((float)$this->getValueDefault('wage.WageAgreementIDCC1747HourRateSunday'), new WageAgreementIDCC1747HourRateSunday($user, $date), self::SUNDAY);
		$this->_addCaltulationHourRateElement((float)$this->getValueDefault('wage.WageAgreementIDCC1747HourRateHS125'), new WageAgreementIDCC1747HourRateHS125($user, $date), self::HS_125);
		$this->_addCaltulationHourRateElement((float)$this->getValueDefault('wage.WageAgreementIDCC1747HourRateHS150'), new WageAgreementIDCC1747HourRateHS150($user, $date), self::HS_150);
		//		$this->_addCaltulationHourRateElement((float) $this->getValueDefault('wage.WageAgreementIDCC1747HourRateHCLess10'),
		//		    new WageAgreementIDCC1747HourRateHCLess10($user, $date));
		$this->_addCaltulationHourRateElement((float)$this->getValueDefault('wage.WageAgreementIDCC1747HourRateHCMore10'), new WageAgreementIDCC1747HourRateHCMore10($user, $date), self::HS_MORE_10);
		$this->_addCaltulationHourRateElement((float)$this->getValueDefault('wage.WageAgreementIDCC1747HourRateNightHours'), new WageAgreementIDCC1747HourRateNightHours($user, $date), self::NIGHT_HOURS);
	}

	private function _filterHourRates($names){
		return array_filter($this->_hourRates, function ($a) use ($names){
			return [];
		});
	}
	/**
	 * @param User $user
	 * @param $date
	 * @param array $names
	 * @return array
	 */
	public function calculateHourRates(User $user, $date, array $names = []) {
		$this->_registerCalculations($user, $date);
		$namesPrepared = $this->_getNames($names, $date);

        $result = [];
		//find calculated
		//with alias in names
		$calculated = array_filter($this->_hourRateElements[$date], function ($a) use ($names){
			return $a['calculated']===true && in_array($a['publicAlias'], $names);
		});
		//find not calculated
		//with alias in names
		$notCalculated = array_filter($this->_hourRateElements[$date], function ($a) use ($names){
			return $a['calculated']!==true && in_array($a['publicAlias'], $names);
		});
		//calculate not calculated
		foreach ($notCalculated as $itemCalculate) {
			//store in calculated array
			$calculated[] = array_replace($itemCalculate,[
				'calculated'=>true,
				'hours'=>$itemCalculate['class']->calculateHourRate(),
				'title'=>$itemCalculate['class']->getTitle(),
				'description'=>$itemCalculate['class']->getDescription(),
			]);
		}
		//store already calculated in hour rateElements
		//and mixed it together with already stored elements
		$this->_hourRateElements[$date] = array_replace(
			$this->_hourRateElements[$date],
			array_combine(array_column($calculated, 'publicAlias'), $calculated)
		);
		return $this->_hourRateElements[$date];
	}

    /**
     * @param $hoursActual
     * @param $date
     * @return float
     */
    public function calculateTotalResultRate($hoursActual, $date) {
        $result = 1;
        if(empty($this->_hourRates[$date])){
            return $result;
        }
        /** implement calculations */
        $devided = $hoursActual * 1;
        $devider = $hoursActual * 1;

        foreach ($this->_hourRates[$date] as $hourRate) {
            if($hourRate['hours']){
                $devided *= (1 + $hourRate['rate']) * $hourRate['hours'];
                $devider *= 1 * $hourRate['hours'];
            }
        }

        if (!empty($devider)) {
            $result = $devided / $devider;
        }
        assert($result >= 1, '!!');
        return $result;
    }

	/**
	 * @param $names
	 * @param $date
	 * @return array
	 */
	private function _getNames($names, $date) {
    	if(empty($names)){ // need all
			return array_values(array_column($this->_hourRateElements[$date], 'publicAlias'));
		}else{
    		return array_values(array_intersect(array_column($this->_hourRateElements[$date], 'publicAlias'), $names));
		}
	}

	/**
	 *
	 */
	public function eraseCalculatedHours() {
		$this->_hourRateElements = [];
	}
}


