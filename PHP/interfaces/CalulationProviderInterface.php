<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 6/1/2017
 * Time: 10:04 AM
 */
interface CalulationProviderInterface
{
	public function getCalculationProvider();
}