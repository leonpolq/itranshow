<?php

/**
 * Implemented DefaultValuesProviderInterface
 */

trait DefaultValuesProviderTrait
{

	protected $_storage;
	/** @var  WageAgreementConfigurationKeys[]  */
	private $_possibleKeys = [];

	public function initDefaultValuesStorage(){
		$possibleKeys = WageAgreementConfigurationKeys::model()
			->findAll();
		$this->_possibleKeys = is_null($possibleKeys) ? [] : $possibleKeys;
		return $this;
	}

	/***
	 * @param $name
	 * @return bool
	 */
	public function hasAccessToValue($name){
		return count(array_filter($this->_possibleKeys, function (WageAgreementConfigurationKeys $keys) use ($name){
			return $keys->name == $name;
		}));
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function hasDefault($name){
		return ($this->hasAccessToValue($name)) && isset($this->_storage[$name]);
	}

	/**
	 * @param $name
	 * @param string $default
	 * @return bool|mixed|string
	 */
	public function getValueDefault($name, $default=''){
		if($this->hasDefault($name)){
			return $this->_storage[$name];
		}elseif(!empty($default)){
			return $default;
		}
		return false;
	}

	/**
	 * @param $name
	 * @param $value
	 * @return $this
	 * @throws Exception
	 */
	public function setValueDefault($name, $value){
		if($this->hasAccessToValue($name)){
			$this->_storage[$name] = $value;
			return $this;
		}else{
			throw new Exception(Yii::t('model', "You should implement key :name to custom wage", [':name'=>$name]));
		}
	}

	/**
	 *
	 */
	public function splitLateHours(){
		if($this->hasAccessToValue('wage.split_late_hour'))
			return (bool) $this->getValueDefault('wage.split_late_hour', true);
		/** Default */
		return true;
	}
	/**
	 *
	 */
	public function isTwoStepsOverworkedHourFullTimeContract(){
		if(
			$this->hasAccessToValue('wage.two_leve_HS')
			&& $this->hasAccessToValue('wage.two_leve_HS_start')
			&& $this->hasAccessToValue('wage.two_leve_HS_middle')
		)
			return (bool) $this->getValueDefault('wage.two_leve_HS', true);
		/** Default */
		return false;
	}
}

