<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 6/1/2017
 * Time: 3:58 PM
 */
class WageAgreementIDCC1747HourRateHS125 implements HourRateElementCalculationInterface
{
	/** @var  User */
	public $_user;
	/** @var  string */
	private $_date;
	public function __construct(User $user, $date) {
		$this->_user=$user;
		$this->_date=$date;
	}
	public function calculateHourRate() {
		$date = new DateTime($this->_date);

        $dateEnd = new DateTime($this->_date);
//        $dateEnd->add(new DateInterval('P1D'));
        $result =
            $this
            ->_user
            ->restaurant
            ->getRestaurantFacture()
            ->callFactureVariableForPeriod('hs_125', $date, $dateEnd, $this->_user);
        if($result){
        	trim('');
		}
		return (float) $result;
	}
    public function getTitle()
    {
        return Yii::t('model', 'HS 125');
    }

    public function getDescription()
    {
        return Yii::t('model', 'HS 125 description');
    }


    public function getDate()
    {
        return $this->_date;
    }
}